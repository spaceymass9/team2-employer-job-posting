import React from 'react';
import {
    Link
} from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark greynav">
            <a className="navbar-brand text-dark p-3"><h1>Header</h1></a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto p-3">
                    <li className="nav-item active">
                        <Link className="nav-link text-dark" to="/home">HOME <span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link text-dark" to="/job-posting">ADD NEW JOB</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link text-dark" to="/browse">BROWSE JOBS</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link text-dark" to="/profile">PROFILE</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link text-dark" to="/log-out">LOG OUT</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Nav;