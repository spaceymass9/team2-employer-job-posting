import React from "react";
import Home from "./pages/home/Home";
import Nav from "./shared/navigation/Nav";
import Footer from "./shared/footer/Footer";
import PostingCard from "./Pages/Browse/PostingCard";
import jobs from "./pages/Browse/jobs";

import { BrowserRouter, Switch, Route } from "react-router-dom";

function Main() {
  return (
    <BrowserRouter>
      <main>
        <Nav />

        <Switch>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/browse">
            <Browse />
          </Route>
          <Route path="/posting">
            <Posting />
          </Route>
        </Switch>

        <Footer />
      </main>
    </BrowserRouter>
  );
}

export default Main;
