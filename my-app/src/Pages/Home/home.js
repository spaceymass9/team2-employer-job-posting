import React from 'react';
import Nav from '../../shared/navigation/Nav';
import Footer from '../../shared/footer/Footer';
import Banner from './Banner';

function Home() {
    return (
        <section>
            <Banner />
        </section>
    );
}

export default Home;