import React from 'react'
import './jobs'
import jobs from './jobs'

function JobsButton(props) {
    return(
        <div class="m-auto">
            <JobsButton 
            label= 'Active Jobs'
            role= 'ADD NEW JOB'
            />
        </div>
    )
}

export default JobsButton