import React from 'react'
import ReactDOM from 'react-dom'
import './jobs'


function PostingCard() {
    return(
        <div className="card text-center mt-3">
        <PostingCard 
        card-title='<a href="/goodwill-classroom/project-demopg3.html">Front End Developer</a>' 
        card-title='Confidential - Charlotte, NC'
        card-text='Strong working experience with HTML5 and web template engines – Markdown, Handlebars, etc.
                Expert level experience with browser-based technology, user interface…' />      
        <PostingCard 
        card-title='<a href="/goodwill-classroom/project-demopg3.html">Junior Web Designer and Front-End Developer</a>'
        card-title='Fame Foundry - Charlotte, NC 28210 (Quail Hollow area)'
        card-text='Fame Foundry is seeking a web designer/front-end developer with a fundamental understanding of full-cycle web development to join our team.'
        />
        
        <PostingCard 
        card-title='<a href="/goodwill-classroom/project-demopg3.html">Junior BI Developer'
        card-title='Discovery Education - Charlotte, NC'
        card-text='A minimum of 6 months experience developing full stack in a web application framework:
        Discovery Education is the global leader in standards-based digital…'

        />
        </div>
    )
};



export default PostingCard