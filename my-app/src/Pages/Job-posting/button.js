import React from 'react';


function Button(props){
	console.log(props.style);
	return(
	<button 
		style= {props.style} 
		className = {props.type=='primary'? 'btn btn-md greynav' : 'btn btn-info btn-md ml-3'}
		onClick= {props.action} > 
		{props.title} 
	</button>)
}


export default Button;